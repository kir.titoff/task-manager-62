package ru.t1.ktitov.tm.service.model;

import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.model.IUserOwnedService;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {
}
