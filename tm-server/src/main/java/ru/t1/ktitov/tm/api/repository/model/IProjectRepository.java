package ru.t1.ktitov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.User;

import java.util.List;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    List<Project> findByUser(@NotNull final User user);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.id = :id AND p.user = :user")
    Project findByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user = :user ORDER BY :orderBy")
    List<Project> findAll(
            @NotNull @Param("user") final User user,
            @NotNull @Param("orderBy") final String orderBy
    );

    @Query("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id AND p.user = :user")
    Boolean existsByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );
    
}
