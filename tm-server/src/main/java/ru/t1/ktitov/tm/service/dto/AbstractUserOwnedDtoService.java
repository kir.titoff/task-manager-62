package ru.t1.ktitov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

}
