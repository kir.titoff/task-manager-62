package ru.t1.ktitov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktitov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M> {

    void clear(@Nullable String userId);

    Boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

}
