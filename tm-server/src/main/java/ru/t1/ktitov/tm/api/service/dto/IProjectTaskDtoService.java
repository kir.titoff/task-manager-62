package ru.t1.ktitov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

public interface IProjectTaskDtoService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    ProjectDTO removeProjectById(@Nullable String userId, @Nullable String projectId);

}
