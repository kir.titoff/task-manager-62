package ru.t1.ktitov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(@Nullable String userId);

    Boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

}
