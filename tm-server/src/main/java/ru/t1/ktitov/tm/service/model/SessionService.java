package ru.t1.ktitov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.model.ISessionRepository;
import ru.t1.ktitov.tm.api.repository.model.IUserRepository;
import ru.t1.ktitov.tm.api.service.model.ISessionService;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class SessionService extends AbstractUserOwnedService<Session>
        implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    public Session add(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Session add(@Nullable String userId, @Nullable Session model) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        model.setUser(user.get());
        return add(model);
    }

    @NotNull
    @Override
    public Collection<Session> add(@Nullable final Collection<Session> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public Session update(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<Session> set(@NotNull final Collection<Session> models) {
        clear();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        return repository.findByUser(user.get());
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        return repository.findByIdAndUser(user.get(), id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id).isPresent();
    }

    @Override
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        return repository.existsByIdAndUser(user.get(), id);
    }

    @NotNull
    @Override
    public Session remove(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    public Session removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<Session> model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @NotNull
    @Override
    public Session removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @Nullable final Session model = repository.findByIdAndUser(user.get(), id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null) throw new EmptyUserIdException();
        @Nullable final List<Session> projects = findAll(userId);
        if (projects == null) throw new ProjectNotFoundException();
        repository.deleteAll(projects);
    }

}
