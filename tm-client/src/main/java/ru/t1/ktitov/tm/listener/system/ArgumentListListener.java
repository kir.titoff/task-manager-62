package ru.t1.ktitov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.model.IListener;
import ru.t1.ktitov.tm.event.ConsoleEvent;
import ru.t1.ktitov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show available arguments";

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = getListeners();
        for (@NotNull final IListener command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
