package ru.t1.ktitov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.ktitov.tm.dto.response.system.ServerVersionResponse;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show application version";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @Nullable final ServerVersionRequest request = new ServerVersionRequest();
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
