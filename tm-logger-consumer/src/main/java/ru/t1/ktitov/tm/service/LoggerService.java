package ru.t1.ktitov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService {

    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    @Autowired
    private MongoDatabase mongoDatabase;

    @SneakyThrows
    public void log(@NotNull final String json) {
        final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
        System.out.println(json);
    }

}
